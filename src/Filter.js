import React from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import HomeIcon from '@material-ui/icons/Home';
import DoneIcon from '@material-ui/icons/Done';
import Badge from '@material-ui/core/Badge';
import UpdateIcon from '@material-ui/icons/Update';

const styles = {
  root: {
    flexGrow: 1,
    width: '100%',
    borderRadius: 4,
    textTransform:'capitalize',
  },
  badge: {
    top: 12,
    right: 15,
  },
  tab: {
    textTransform:'capitalize',
    timeout: 3000 
  }
};

class IconTabs extends React.Component {
  state = {
    checked: false,
  };
  constructor(props)
  {
    super(props);
  } 

  selectTask = (event, value) => {
    const {filter} = this.props;
    filter(value);
}
  render() {
    const { classes, numberTask } = this.props;
    return (
      <Paper square className={classes.root}>
        <Tabs
          value={this.state.value}
          onChange={this.selectTask}
          fullWidth
          indicatorColor="primary"
          textColor="primary"
        >
          <Tab className={classes.tab} icon={<HomeIcon />} value="all" label="Все задачи" />
          <Tab className={classes.tab} icon={<Badge  badgeContent={numberTask('active')} color="primary"><UpdateIcon /></Badge>} value="active" label="В процессе" />
          <Tab className={classes.tab} icon={<Badge  badgeContent={numberTask('done')} color="primary"><DoneIcon /></Badge>} value="done" label="Выполненные" />
        </Tabs>
      </Paper>
    );
  }
}

IconTabs.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(IconTabs);
