import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import Input from '@material-ui/core/Input';
import Paper from '@material-ui/core/Paper';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';

const styles = theme => ({
    button: {
      margin: 5,
    },
    container: {
        display: 'flex',
        flexWrap: 'nowrap',
        marginBottom: 5,
        marginTop: 10,
      },
      input: {
        margin: 0,
        width: '100%',
        background: 'none'
      },
      checkbox: {
          marginLeft: 0,
      }
  });

class Art extends React.Component {
    state = {
        task: '',
    }
    constructor(props)
  {
    super(props);
  }  
    checkBlock = () => {
        const {datas, check} = this.props;
        check(datas);
    }
    disableBlock = () => {
        const {datas, funDel} = this.props;
        funDel(datas);
    }
    editBlock = (e) => {    
        e.preventDefault()
        const {datas, edit} = this.props;
        edit(datas, e.currentTarget.value);
    }
    render() {
        let {name} = this.props.elArr;
        const {checkBox} = this.props;
        const { classes } = this.props;
        return(
            <div className='newsBlock'>
                <Paper className={classes.container} elevation={1}>
                <Checkbox value="checkedC" checked={checkBox} className={classes.checkbox} onChange={this.checkBlock} color="primary" />
                <Input
                    className={classes.input}
                    type='text'
                    value={name}
                    onChange={this.editBlock}
                    variant="filled"
                    disableUnderline
                        />
                <IconButton aria-label="Delete" className={classes.button} onClick={this.disableBlock}>
                    <DeleteIcon fontSize="small" />
                </IconButton>
                </Paper>
            </div>
        )
    }
}
  
  export default withStyles(styles)(Art)