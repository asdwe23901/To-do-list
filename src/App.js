import React, { Component } from 'react';
// import Index from './src/pages/index';
import Add from './Add';
import Task from './Task';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import './App.css';
import { Paper } from '@material-ui/core';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  demo: {
    height: 'auto',
  },
  paper: {
    padding: theme.spacing.unit * 2,
    height: '100%',
    minWidth: '540px',
    color: theme.palette.text.secondary,
  },
  control: {
    padding: theme.spacing.unit * 2,
  },
});

class App extends Component {
  state = {
    text: [
    ],
    filter: 'all',
  }
  constructor(props)
  {
    super(props);
  }  
  filterChange = (e) => {
    this.setState({filter: e});
  }
  selectGroupTask = (item, arg) => {
  
    switch(arg) {
      case 'active': 
      return item.filter(function(item) {
        return !item.checked
      }); 
      case 'done': 
      return item.filter(function(item) {
        return item.checked
      }); 
      case 'all': 
        return item
        case 'activeBadge':
        return item.filter(function(item) {
          return item.checked
        }).length; 
      default:
       return item;
    }
  }
  checkTask = (arg) => {
    let checkNews = this.state.text;
      checkNews.forEach(function(item, i, array) {
          if( item.id === arg ) {
            return item.checked = !item.checked
          }
      })
    this.setState({text: checkNews})
  }
  deleteTask = (arg) => {
    let deleteNews = this.state.text;
      deleteNews.forEach(function(item, i, array) {
          if( item.id === arg ) {
            deleteNews.splice(i, 1);
          }
      })
      this.setState({text: deleteNews})
  }
  editTask = (arg, name) => {
    let editNews = this.state.text;
      editNews.forEach(function(item, i, array) {
          if( item.id === arg ) {
            return item.name = name;
          }
      })
      this.setState({text: editNews})
  }
  addTask = (data) => {
    const addNews = [data, ...this.state.text];
    this.setState({text: addNews})
  }
  numberTask = arg => {
    let st = this.state.text;
    if (arg === 'done') {
      st =  st.filter(function(item) {
        return item.checked
      }); 
      return st.length;
    }
    else if (arg === 'active') {
      st =  st.filter(function(item) {
        return !item.checked
      }); 
      return st.length;
    }
  }
  render() {
    const {text, filter} = this.state;
    const filterFunc = this.selectGroupTask(text, filter);
    const filterAdd = this.filterChange;
    const { classes } = this.props;
    return (
      <React.Fragment>
        <CssBaseline />
        <Grid container className={classes.root} >
          <Grid item xs={12} >
            <Grid
              container
              spacing={12}
              className={classes.demo}
              direction="column"
              justify="center"
              alignItems="center"
            >
                <Add filter={filterAdd} filterEl={filter} data={this.addTask} update={this.updateComp}/>
            </Grid>
          </Grid>
          <Grid item xs={12} >
            <Grid
              container
              spacing={12}
              className={classes.demo}
              direction="column"
              justify="center"
              alignItems="center"
            > 
            <div className={classes.paper}  >
                <Task numberTask={this.numberTask}  filter={filterAdd} filterEl={filter} data={filterFunc} dels={this.deleteTask} edit={this.editTask} check={this.checkTask}/>
                </div>
                
            </Grid>
          </Grid>
        </Grid>
      </React.Fragment>
    )
  }
}



export default withStyles(styles)(App);
