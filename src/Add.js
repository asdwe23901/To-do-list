import React from 'react'
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import CreateIcon from '@material-ui/icons/Create';
import SearchIcon from '@material-ui/icons/Search';
import SendIcon from '@material-ui/icons/Send';
import InputBase from '@material-ui/core/InputBase';
import { fade } from '@material-ui/core/styles/colorManipulator';
import Button from '@material-ui/core/Button';

const styles = theme => ({
    root: {
      width: '100%',
    },
    grow: {
      flexGrow: 1,
    },
    menuButton: {
      marginLeft: -12,
      marginRight: 20,
    },
    title: {
      display: 'none',
      marginLeft: 0,
      [theme.breakpoints.up('sm')]: {
        display: 'block',
      },
    },
    search: {
      position: 'relative',
      borderRadius: theme.shape.borderRadius,
      backgroundColor: fade(theme.palette.common.white, 0.15),
      '&:hover': {
        backgroundColor: fade(theme.palette.common.white, 0.35),   
      },
      marginRight: theme.spacing.unit * 2,
      marginLeft: 0,
      width: '100%',
      [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing.unit * 3,
        width: 'auto',
      },
    },
    searchIcon: {
      width: theme.spacing.unit * 9,
      height: '100%',
      position: 'absolute',
      pointerEvents: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    inputRoot: {
      color: 'inherit',
      width: '100%',
    },
    inputInput: {
      paddingTop: theme.spacing.unit,
      paddingRight: theme.spacing.unit,
      paddingBottom: theme.spacing.unit,
      paddingLeft: theme.spacing.unit * 10,
      transition: theme.transitions.create('width'),
      width: '100%',
      [theme.breakpoints.up('md')]: {
        width: 200,
      },
    },

    tabsIndicator: {
        backgroundColor: '#ffffff',
        borderBottom: 'none',
        margin: 0,
      },
      tabRoot: {
        textTransform: 'initial',
        color: '#F5F5F5',
        backgroundColor: '#3f51b5',
        margin: 0,
        width: 20,
        '&:hover': {
          color: '#ffffff',
          opacity: 1,
          backgroundColor: '#a31545',
        },
        '&$tabSelected': {
          color: '#ffffff',
          backgroundColor: '#a31545',
    },
        '&:focus': {
          backgroundColor: '#a31545',
          color: '#ffffff',    
        },
      },
    tabSelected: {},
    tabsRoot: {
        display: 'flex',
        width: 'auto'
      },
  });

class Add extends React.Component {
    state = {
        taskText: '',
        id: '',
        checked: false,
    }
    constructor(props)
    {
      super(props);
    } 
    selectTask = (event, value) => {
        const {filter} = this.props;
        filter(value);
    }
    editTask = (e) => {
        e.preventDefault()
            this.setState({taskText: e.currentTarget.value})
    }
    sendTask = (e) => {
        e.preventDefault()
        const { taskText, checked } = this.state;
        if (this.state.taskText == 0) this.setState({taskText: ''})
        else this.props.data({
            name: taskText,
            id: +new Date(),
            checked: checked
        });
    this.setState({taskText: ''})
        
    }
    render() {
        const { classes } = this.props;
        return(
            <div className={classes.root}>
        <AppBar position="static" color='primary'>
          <Toolbar  style={{margin: 'auto', flexGrow: 0}}  >
            <Typography className={classes.title} variant="h6" color="inherit" noWrap>
                Alf's To-do list
            </Typography>
            <form action=''>
                <div className={classes.search}>
                <div className={classes.searchIcon}>
                    <CreateIcon />
                </div>
                <InputBase
                    placeholder="Add.."
                    onChange={this.editTask}
                    value={this.state.taskText}

                    classes={{
                    root: classes.inputRoot,
                    input: classes.inputInput,
                    }}
                />
                </div>
                <Button type="submit" label="login" size="small" className={classes.button} color="inherit" onClick={this.sendTask} primary={true}>
                    <SendIcon className={classNames(classes.rightIcon, classes.iconSmall)}>send</SendIcon>
                </Button>
            </form>  
          </Toolbar>
          
        </AppBar>
        
      </div>
        )
    }
}

export default withStyles(styles)(Add);