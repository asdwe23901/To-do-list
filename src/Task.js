import React from 'react';
import Art from './Art';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import Filter from './Filter';

const styles = theme => ({
    root: {
      backgroundColor: theme.palette.background.paper,
    },
  });


class Task extends React.Component {
    state = {
        vision: true,
    }
    showNews = () => {
        const {data, dels, edit, check} = this.props;
        
        let listNews = data.map(function(item) {
            return <Art key={item.id} elArr={item} datas={item.id} data={data} funDel={dels} edit={edit} check={check} checkBox={item.checked}/>
        })
        return listNews;
    }
    render() {
        
        return(
            <List>
                <Filter filter={this.props.filter} numberTask={this.props.numberTask}/>
                {this.showNews()}
            </List>
        )
    }
}

export default Task;